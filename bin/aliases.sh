
bind 'set enable-bracketed-paste off'

#alias sudo='sudo -HE env PATH=${PATH}'
alias size='sudo du -hs'
alias sizel='sudo du -h  --max-depth=1 | sort -n'
alias cha='chmod -R a+x *'
alias path='echo -e ${PATH//:/\\n}'
alias now='date +"%T'
alias psmem30='ps auxf | sort -nr -k 4 | head -30'
alias version='lsb_release -a'
alias update='sudo apt-get update --fix-missing; sudo apt-get upgrade -y '
alias updatef='sudo apt-get update --fix-missing; sudo apt-get upgrade -y'
alias apts='aptitude search '
alias ll='ls -la'
alias mkdir='mkdir -pv'
alias path='echo -e ${PATH//:/\\n}'
alias now='date +"%T'
alias nowtime=now
alias nowdate='date +"%d-%m-%Y"'
alias ping='ping -c 5'
alias ports='netstat -tulanp'
alias psmem30='ps auxf | sort -nr -k 4 | head -30'
alias meminfo='free -m -l -t'
alias install='sudo apt-get install '
alias remove='sudo apt-get --purge remove '
alias ipaddress='/sbin/ifconfig'
alias clean='df -h;sudo apt-get --purge autoremove && sudo apt-get clean && sudo apt-get autoclean && sudo apt-get clean;sudo rm -rf /home/dpss/.cache/*; sudo rm -rf /home/sweety/.cache/*;df -h; echo 3 | sudo tee /proc/sys/vm/drop_caches;free -m'
alias tarz='tar -cvzf '
alias hsearch='history |grep -i '
alias smain='screen -rd main'
alias smain1='screen -rd main1'
alias smain2='screen -rd main2'
alias smain3='screen -rd main3'
alias smain4='screen -rd main4'
alias smain5='screen -rd main5'
alias smain6='screen -rd main6'
alias smain7='screen -rd main7'
alias smain8='screen -rd main8'
alias smain9='screen -rd main9'
alias sip='sudo asterisk -rvvv'
alias myip='wget -O - http://icanhazip.com/ -o /dev/null'
alias lsports='sudo netstat -tulpn'
alias Smain='screen -S main'
alias Smain1='screen -S main1'
alias Smain2='screen -S main2'
alias Smain3='screen -S main3'
alias Smain4='screen -S main4'
alias Smain5='screen -S main5'
alias Smain6='screen -S main6'
alias Smain7='screen -S main7'
alias Smain8='screen -S main8'
alias Smain9='screen -S main9'
alias exim_list='sudo exim -bp '
alias iptable_status='sudo iptables -L'
alias iptable_clear='sudo iptables -P INPUT ACCEPT; sudo iptables -P FORWARD ACCEPT;sudo iptables -P OUTPUT ACCEPT;sudo iptables -t nat -F; sudo iptables -t mangle -F; sudo iptables -F; sudo iptables -X'
alias ctop='sudo ctop'
alias docker-update='docker-compose pull && docker-compose up -d'
alias rootsize='sudo find / -maxdepth 1 -type d  -and -not -path /home -and -not -path /proc -and -not -path /mnt -and -not -path /dev -and -not -path / -exec sudo du -ch {} + | tail -n 1'
alias dockers_update='docker run --rm -v /var/run/docker.sock:/var/run/docker.sock containrrr/watchtower --run-once'
alias dockers_clean='sudo docker system prune -a -f'







